# Langchain Agents

```bash
python3 -m venv env
source env/bin/activate

pip3 install -r requirements.txt

## Run agent
bash scripts/dev.sh

## Run a specific script
bash scripts/question.sh $PATH
```