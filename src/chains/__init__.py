import os
from langchain import (
    OpenAI, 
    SerpAPIWrapper, 
    # LLMChain, 
    LLMMathChain, 
    SQLDatabase, 
    SQLDatabaseChain, 
    LLMBashChain,
)
from langchain.chains import APIChain
from langchain.agents import Tool
from src.chains.docs.skrumify.events.create import SKRUMIFY_EVENT_CREATE
from src.chains.docs.issues import GITHUB_ISSUE_DOCS, GITLAB_ISSUE_DOCS

VERBOSE = True

OPENAI_API_KEY = os.getenv('OPENAI_API_KEY')
SERP_API_KEY = os.getenv('SERP_API_KEY')

llm = OpenAI(temperature=0, openai_api_key=OPENAI_API_KEY)
search = SerpAPIWrapper(serpapi_api_key=SERP_API_KEY)
llm_math_chain = LLMMathChain(llm=llm, verbose=VERBOSE)
db = SQLDatabase.from_uri("sqlite:///notebooks/Chinook.db")
db_chain = SQLDatabaseChain(llm=llm, database=db, verbose=VERBOSE)
bash_chain = LLMBashChain(llm=llm, verbose=VERBOSE)

##################################################################
##  GITHUB ISSUES
##################################################################
# headers = {"Authorization": f"Bearer {os.environ['TOKEN']}"}
github_issue_chain = APIChain.from_llm_and_api_docs(
    llm, 
    GITHUB_ISSUE_DOCS, 
    # headers, 
    verbose=VERBOSE
)


##################################################################
##  GITLAB ISSUES
##################################################################
gitlab_headers = {
    'PRIVATE-TOKEN': os.environ['GITLAB_TOKEN'],
    'Content-Type': 'application/json'
}
gitlab_issue_chain = APIChain.from_llm_and_api_docs(
    llm, 
    GITLAB_ISSUE_DOCS, 
    gitlab_headers, 
    verbose=VERBOSE
)

##################################################################
##  GITLAB ISSUES
##################################################################
SKRUMIFY_JWT = os.environ['SKRUMIFY_JWT']
skrumify_headers = {
    'Authorization': f'Bearer {SKRUMIFY_JWT}',
    'Content-Type': 'application/json'
}
skrumify_event_create_chain = APIChain.from_llm_and_api_docs(
    llm, 
    SKRUMIFY_EVENT_CREATE, 
    skrumify_headers, 
    verbose=VERBOSE
)

class Chain:
    def __init__(self):
        self.DEFAULT = llm
        self.SEARCH = search
        self.MATH = llm_math_chain
        self.DB = db_chain
        self.BASH = bash_chain
        self.GITHUB_ISSUES = github_issue_chain
        self.GITLAB_ISSUES = gitlab_issue_chain
        self.SKRUMIFY_EVENT_CREATE = skrumify_event_create_chain
        
TOOLS = [
    Tool(
        name = "Search",
        func=search.run,
        description="useful for when you need to answer questions about current events. You should ask targeted questions"
    ),
    Tool(
        name="Calculator",
        func=llm_math_chain.run,
        description="useful for when you need to answer questions about math"
    ),
    Tool(
        name="FooBar DB",
        func=db_chain.run,
        description="useful for when you need to answer questions about FooBar. Input should be in the form of a question containing full context"
    ),
    Tool(
        name="Bash",
        func=Chain().BASH.run,
        description="useful for executing bash commands to the local console",
        return_direct=True
    ),
    Tool(
        name="APIChain - Github Issues",
        func=Chain().GITHUB_ISSUES.run,
        description="useful for retrieving information about issues from a Github repository"
    ),
    Tool(
        name="APIChain - Gitlab Issues",
        func=Chain().GITLAB_ISSUES.run,
        description="useful for retrieving information about issues from a Gitlab repository"
    ),
    Tool(
        name="APIChain - Skrumify Event Create",
        func=Chain().SKRUMIFY_EVENT_CREATE.run,
        description="useful for creating events in the Skrumify calendar"
    ),
]