GITHUB_ISSUE_DOCS = """API documentation:
Endpoint: https://api.github.com
GET /repos/{owner}/{repo}/issues

This API is for returning all issues from a github repository.

Path paramters table:
Attribute,Type,Description
owner,String,The account owner of the repository. The name is not case sensitive.
repo,String,The name of the repository. The name is not case sensitive.                                                                   |

Query parameters table:
Attribute,Type,Description
milestone,String,If an integer is passed, it refers to a milestone by its number field. If the string * is passed, issues with any milestone are accepted. If the string none is passed, issues without milestones are returned.
state,String,Indicates the state of the issues to return. Default value is open. Can be one of: open, closed, all.
assignee,String,Can be the name of a user. Pass in none for issues with no assigned user, and * for issues assigned to any user.
creator,String,The user that created the issue.
mentioned,String,A user that's mentioned in the issue.
labels,String,A list of comma separated label names. Example: bug,ui,@high.
sort,String,What to sort results by. Default value is created. Can be one of: created, updated, comments.
direction,String,The direction to sort the results by. Default value is desc. Can be one of: asc, desc.
since,String,Only show notifications updated after the given time. This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ.
per_page,Integer,The number of results per page (max 100). Default value is 30.
page,Integer,Page number of the results to fetch. Default value is 1.


Response Object Table:
Attribute,Type,Description
id,integer,Unique identifier for the issue
node_id,string,Unique identifier for the issue (as a string)
url,string,URL linking to the issue
repository_url,string,URL linking to the repository containing the issue
labels_url,string,URL linking to the labels assigned to the issue
comments_url,string,URL linking to the comments on the issue
events_url,string,URL linking to the events related to the issue
html_url,string,URL linking to the issue on GitHub
number,integer,The issue number
state,string,The state of the issue (open, closed, etc.)
title,string,The title of the issue
body,string,The description of the issue
user,object,User who created the issue
labels,array,Labels assigned to the issue
assignee,object,User assigned to work on the issue
assignees,array,Users assigned to work on the issue
milestone,object,The milestone associated with the issue (if one is assigned)
locked,boolean,Whether or not the issue is locked
active_lock_reason,string,Reason why the issue is locked (if locked)
comments,integer,Number of comments on the issue
pull_request,object,Pull request associated with the issue (if one is associated)
closed_at,string,Timestamp for when the issue was closed (if closed)
created_at,string,Timestamp for when the issue was created
updated_at,string,Timestamp for when the issue was last updated"""


GITLAB_ISSUE_DOCS = """API documentation:
Endpoint: https://gitlab.com/api/v4/projects/{project_id}

This API is for returning all issues from a Gitlab repository.

Query parameters:
GET /issues
GET /issues?assignee_id=5
GET /issues?author_id=5
GET /issues?confidential=true
GET /issues?iids[]=42&iids[]=43
GET /issues?labels=foo
GET /issues?labels=foo,bar
GET /issues?labels=foo,bar&state=opened
GET /issues?milestone=1.0.0
GET /issues?milestone=1.0.0&state=opened
GET /issues?my_reaction_emoji=star
GET /issues?search=foo&in=title
GET /issues?state=closed
GET /issues?state=opened

Response Object Table:
Name,Type,Description
state,string,The current state of the issue
description,string,The description of the issue
author,object,The author of the issue
author.state,string,The current state of the author
author.id,integer,The unique identifier of the author
author.web_url,string,The web URL of the author's GitLab profile
author.name,string,The name of the author
author.avatar_url,string/null,The URL of the author's avatar or null if not available
author.username,string,The username of the author
milestone,object/null,The milestone associated with the issue, or null if none
milestone.project_id,integer,The ID of the project the milestone belongs to
milestone.description,string,The description of the milestone
milestone.state,string,The current state of the milestone
milestone.due_date,string/null,The due date of the milestone or null if not set
milestone.iid,integer,The internal ID of the milestone
milestone.created_at,string,The date/time the milestone was created
milestone.title,string,The title of the milestone
milestone.id,integer,The unique identifier of the milestone
milestone.updated_at,string,The date/time the milestone was last updated
project_id,integer,The ID of the project the issue belongs to
assignees,array,The users assigned to the issue
assignees.state,string,The current state of the assignee
assignees.id,integer,The unique identifier of the assignee
assignees.name,string,The name of the assignee
assignees.web_url,string,The web URL of the assignee's GitLab profile
assignees.avatar_url,string/null,The URL of the assignee's avatar or null if not available
assignees.username,string,The username of the assignee
assignee,object/null,The user assigned to the issue, or null if none
assignee.state,string,The current state of the assignee
assignee.id,integer,The unique identifier of the assignee
assignee.name,string,The name of the assignee
assignee.web_url,string,The web URL of the assignee's GitLab profile
assignee.avatar_url,string/null,The URL of the assignee's avatar or null if not available
assignee.username,string,The username of the assignee
type,string,The type of the issue
updated_at,string,The date/time the issue was last updated
closed_at,string/null,The date/time the issue was closed or null if still open
closed_by,object/null,The user who closed the issue, or null if not closed
closed_by.state,string,The current state of the user who closed the issue
closed_by.id,integer,The unique identifier of the user who closed the issue
closed_by.name,string,The name of the user who closed the issue"""
