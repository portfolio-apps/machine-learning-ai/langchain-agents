SKRUMIFY_EVENT_CREATE = """API documentation:
Endpoint: https://ts-prod-api.glootie.ml/api/v1
POST /events

Request Body Parameters Table:
Field,Type,Description
title,string,The title of the event.
start,integer,The start time of the event in Unix timestamp format.
end,integer,The end time of the event in Unix timestamp format.
bgColor,string,The background color of the event.
location,string,The location of the event.
description,string,A description of the event.

Response Object Table:
Field,Type,Description
success,boolean,Indicates if the request was successful or not.
event.title,string,The title of the event.
event.start,string,The start time of the event in ISO 8601 format.
event.bgColor,string,The background color of the event.
event.end,string,The end time of the event in ISO 8601 format.
event.location,string,The location of the event.
event.description,string,A description of the event.
event.user,string,The unique identifier of the user who created the event.
event.notifications,array,An array containing notifications for the event.
event._id,string,The unique identifier of the event.
event.createdAt,string,The date and time when the event was created in ISO 8601 format.
event.updatedAt,string,The date and time when the event was last updated in ISO 8601 format.
"""