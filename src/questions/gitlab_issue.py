import sys
from pathlib import Path
path_root = Path(__file__).parents[2]
sys.path.append(str(path_root))

from langchain.chat_models.openai import ChatOpenAI
from langchain.agents.chat.base import ChatAgent
from langchain.agents import AgentExecutor, Tool
from src.chains import OPENAI_API_KEY, VERBOSE, Chain

#####################################################
##  Tools
#####################################################
TOOLS = [
    Tool(
        name="APIChain - Github Issues",
        func=Chain().GITHUB_ISSUES.run,
        description="useful for retrieving information about issues from a Github repository"
    ),
    Tool(
        name="APIChain - Gitlab Issues",
        func=Chain().GITLAB_ISSUES.run,
        description="useful for retrieving information about issues from a Gitlab repository"
    ),
]

#####################################################
##  Agent
#####################################################
agent = ChatAgent.from_chat_model_and_tools(ChatOpenAI(temperature=0, openai_api_key=OPENAI_API_KEY), TOOLS)
agent_executor = AgentExecutor.from_agent_and_tools(agent=agent, tools=TOOLS, verbose=VERBOSE)

#####################################################
##  Prompts
#####################################################
# answer = agent_executor.run("""SYSTEM: You are a friendly coding assistant.\n\n
#                             USER: Get issues for kre8mymedia static repo?\n
#                             AI: The issue for kre8mymedia static repo is titled "Need more imagess" and is currently open with no assigned user, labels, or milestone. There are no comments on the issue.\n\n
#                             USER: When was the issue created?""")

answer = agent_executor.run(f"""System: You are a Gitlab assistant.\n\n
                            User: On project 22626851 issue 86, analyze the title and generate a description.""")

print(answer)