import os
import sys
from pathlib import Path
path_root = Path(__file__).parents[2]
sys.path.append(str(path_root))

from datetime import datetime
import pytz
ts = datetime.now().timestamp()
tz = pytz.timezone('America/Chicago')
iso = datetime.fromtimestamp(int(ts), tz).isoformat()
dt = datetime.fromisoformat(iso)
formatted_string = dt.strftime("%d/%m/%Y %I:%M %p")

from langchain.chat_models.openai import ChatOpenAI
from langchain.agents.chat.base import ChatAgent
from langchain.agents import AgentExecutor, Tool
from src.chains import OPENAI_API_KEY, VERBOSE, Chain
from src.chains.docs.skrumify.events.create import SKRUMIFY_EVENT_CREATE

SKRUMIFY_JWT = os.environ['SKRUMIFY_JWT']
print(iso)

#####################################################
##  Tools
#####################################################
TOOLS = [
    Tool(
        name="APIChain - Skrumify Event Create",
        func=Chain().SKRUMIFY_EVENT_CREATE.run,
        description="useful for creating events in the Skrumify calendar"
    ),
]

#####################################################
##  Agent
#####################################################
agent = ChatAgent.from_chat_model_and_tools(ChatOpenAI(temperature=0, openai_api_key=OPENAI_API_KEY), TOOLS)
agent_executor = AgentExecutor.from_agent_and_tools(agent=agent, tools=TOOLS, verbose=VERBOSE)


#####################################################
##  Prompts
#####################################################
question = input("What would you like added to calendar? ")

answer = agent_executor.run(f"""SYSTEM: {SKRUMIFY_EVENT_CREATE}, Bearer Auth: {SKRUMIFY_JWT}, Current Time: {iso}\n\n
                            USER: Return cURL request to create an event using the Skrumify API. {question} The start and end properties in request body should in YYYY-MM-DDTHH:MM""")

# print(answer)