import sys
from pathlib import Path
path_root = Path(__file__).parents[2]
sys.path.append(str(path_root))

from langchain.chat_models.openai import ChatOpenAI
from langchain.agents.chat.base import ChatAgent
from langchain.agents import AgentExecutor, Tool
from src.chains import OPENAI_API_KEY, VERBOSE, Chain

#####################################################
##  Tools
#####################################################
TOOLS = [
    Tool(
        name = "Search",
        func=Chain().SEARCH.run,
        description="useful for when you need to answer questions about current events. You should ask targeted questions"
    ),
    Tool(
        name="Calculator",
        func=Chain().MATH.run,
        description="useful for when you need to answer questions about math"
    ),
]

#####################################################
##  Agent
#####################################################
agent = ChatAgent.from_chat_model_and_tools(ChatOpenAI(temperature=0, openai_api_key=OPENAI_API_KEY), TOOLS)
agent_executor = AgentExecutor.from_agent_and_tools(agent=agent, tools=TOOLS, verbose=VERBOSE)

#####################################################
##  Prompts
#####################################################
answer = agent_executor.run("""SYSTEM: You are a friendly assistant.\n\n
                            USER: Who won the 2001 world series?\n
                            AI: The Arizona Diamondbacks won the 2001 World Series.\n\n
                            USER: Who were the pitchers?""")

print(answer)