
import sys
from pathlib import Path
path_root = Path(__file__).parents[2]
sys.path.append(str(path_root))

from langchain.chat_models.openai import ChatOpenAI
from langchain.agents.chat.base import ChatAgent
from langchain.agents import AgentExecutor
from src.chains import TOOLS, OPENAI_API_KEY, VERBOSE

# question = input("Question: ")
        
agent = ChatAgent.from_chat_model_and_tools(ChatOpenAI(temperature=0, openai_api_key=OPENAI_API_KEY), TOOLS)
agent_executor = AgentExecutor.from_agent_and_tools(agent=agent, tools=TOOLS, verbose=VERBOSE)

# agent_executor.run("""Question:  Can you get a list for houses in the zip code 76227?\n
#                    Answer: There are 847 homes for sale in zip code 76227 with a median listing price of $425,000.\n\n
#                    """)

# answer = agent_executor.run("""System: You are a coding assistant, I will ask you to create files and you will create files using the command line and NOT nano.\n\n
#                             User: Create a index.js file, it should console.log('Hello World!'). Print DONE when finished.""")

# answer = agent_executor.run(f"""System: You are a Gitlab assistant.\n\n
#                             User: On project 42350176 issue 9, analyze the title and generate a description.""")

# answer = agent_executor.run("""SYSTEM: You are a friendly assistant.\n\n
#                             USER: Who won the 2001 world series?\n
#                             AI: The Arizona Diamondbacks won the 2001 World Series.\n\n
#                             USER: Who were the pitchers?""")

# answer = agent_executor.run("""SYSTEM: You are a friendly coding assistant.\n\n
#                             USER: Get issues for kre8mymedia static repo?\n
#                             AI: The issue for kre8mymedia static repo is titled "Need more imagess" and is currently open with no assigned user, labels, or milestone. There are no comments on the issue.\n\n
#                             USER: When was the issue created?""")

# answer = agent_executor.run("""SYSTEM: You are a friendly assistant.\n\n
#                             USER: Use markdown to create and download as pdf.""")


# print(answer)
