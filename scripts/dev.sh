#!/bin/bash

DIR=$(pwd)

### Set Environment Variables
set -a # automatically export all variables
source .env
set +a


python3 $DIR/src/agents/chatgpt.py